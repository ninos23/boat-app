package com.open.boat;

import com.open.boat.dao.BoatRepository;
import com.open.boat.entities.Boat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

//@DataJpaTest
@SpringBootTest
class BoatApplicationTests {

    @Mock
    private BoatRepository boatRepository;

    private Boat boat;
    private List<Boat> boatList = new ArrayList<>();

    @BeforeEach
    public void setUp() throws RuntimeException {
        boat = new Boat(33l, "boat33", "boat33 Description");
        boatList.add(boat);
        boatList.add(new Boat(34l, "boat34", "boat34 Description"));
    }

    @Test
    public void contextLoads() throws RuntimeException {
        assertThat(boatRepository).isNotNull();
    }


    @Test
    public void getAllTest() throws RuntimeException {
        Mockito.when(boatRepository.findAll()).thenReturn(boatList);
        List<Boat> boatsList2 = boatRepository.findAll();
        assertThat(boatList).isEqualTo(boatsList2);
    }

    @Test
    public void getByIdTest() throws RuntimeException {
        Mockito.when(boatRepository.findById(anyLong())).thenReturn(Optional.ofNullable(boat));
        Boat boat33 = boatRepository.findById(33L).orElse(null);
        assertThat(boat).isEqualTo(boat33);
    }

    @Test
    public void addTest() throws RuntimeException {
        Mockito.when(boatRepository.save(any())).thenReturn(boat);
        Boat boat33 = boatRepository.save(boat);
        assertThat(boat).isEqualTo(boat33);
    }

    @Test
    public void updateTest() throws RuntimeException {
        Mockito.when(boatRepository.save(any())).thenReturn(boat);
        boat.setName("New boat test");
        Boat boatUpdated = boatRepository.save(boat);
        assertThat("New boat test").isEqualTo(boatUpdated.getName());
    }

    @Test
    public void deleteTest() throws RuntimeException {
        Optional<Boat> optionalBoatType = Optional.of(boat);
        Mockito.when(boatRepository.findById(anyLong())).thenReturn(optionalBoatType);
        boatRepository.delete(boat);
        Mockito.verify(boatRepository).delete(boat);
    }


}
