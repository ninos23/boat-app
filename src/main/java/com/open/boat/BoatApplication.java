package com.open.boat;

import com.open.boat.dao.BoatRepository;
import com.open.boat.entities.AppRole;
import com.open.boat.entities.AppUser;
import com.open.boat.entities.Boat;
import com.open.boat.services.AccountService;
import org.apache.catalina.filters.CorsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@SpringBootApplication
public class BoatApplication implements CommandLineRunner {

    @Autowired
    private BoatRepository boatRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RepositoryRestConfiguration repositoryRestConfiguration;

    public static void main(String[] args) {
        SpringApplication.run(BoatApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        //ADD ID to the Response
        repositoryRestConfiguration.exposeIdsFor(Boat.class);

        // Generate data on H2 DataBase
        boatRepository.save(new Boat(null, "boat1", "boat1 description"));
        boatRepository.save(new Boat(null, "boat2", "boat2 description"));
        boatRepository.save(new Boat(null, "boat3", "boat3 description"));

        accountService.saveUser(new AppUser(null, "admin", "1234", null));
        accountService.saveUser(new AppUser(null, "user1", "1234", null));

        accountService.saveRole((new AppRole(null, "ADMIN")));
        accountService.saveRole((new AppRole(null, "USER")));

        accountService.addRoleToUser("admin", "ADMIN");
        accountService.addRoleToUser("admin", "USER");
        accountService.addRoleToUser("user1", "USER");

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
