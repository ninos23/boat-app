package com.open.boat.security.jwt;

public class SecurityConstants {
    public static final String SECRET = "KA01234bo";
    public static final long EXPIRATION_TIME = 1*60*1000; // 1 minutes
    public static final long REFRESH_EXPIRATION_TIME = 10*24*60*60*1000;// 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTHORIZATION = "Authorization";
}
