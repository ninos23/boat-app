package com.open.boat.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.open.boat.entities.AppUser;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        //dans le cas où login et pass sont envoyé en format urlEncoded au lieu du json
        //String userNmae = request.getParameter("username");

        //Format d'authentification : Json
        try {
            AppUser appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(appUser.getUserName(), appUser.getPassword()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        User springUser = (User) authResult.getPrincipal();


        //dans l'idéal ne generer un nouveau token que s'il n'est pas blackliste
        //Generer le Token
        Algorithm algorithm = Algorithm.HMAC256(SecurityConstants.SECRET);
        String jwt = JWT.create()
                .withSubject(springUser.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",
                        springUser.getAuthorities().stream()
                                .map(ga -> ga.getAuthority())
                                .collect(Collectors.toList()))
                .sign(algorithm);

        String jwtRefreshToekn = JWT.create()
                .withSubject(springUser.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.REFRESH_EXPIRATION_TIME))
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);
        Map<String, String> idTokens = new HashMap<>();
        idTokens.put("accesstoken", SecurityConstants.TOKEN_PREFIX + jwt);
        idTokens.put("refreshtoken", SecurityConstants.TOKEN_PREFIX + jwtRefreshToekn);
        response.setContentType("application/json");
        new ObjectMapper().writeValue(response.getOutputStream(),idTokens);

        //response.setHeader(SecurityConstants.AUTHORIZATION, SecurityConstants.TOKEN_PREFIX + jwt);

    }

}
