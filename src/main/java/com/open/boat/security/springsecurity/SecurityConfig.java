package com.open.boat.security.springsecurity;

import com.open.boat.entities.AppUser;
import com.open.boat.security.jwt.JWTAuthenticationFilter;
import com.open.boat.security.jwt.JWTAuthorizationFilter;
import com.open.boat.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private  UserDetailsServiceImpl userDetailsService;

    private static final String BOATS_PATH = "/boats/**";
    private static final String ADMIN = "ADMIN";

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        //TODO
        //frame h2 database à enlever pour l'env prod
        http.headers().frameOptions().disable();
        //http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
        http.formLogin().disable();

        //Authorisation Verb
        http.authorizeRequests().antMatchers(HttpMethod.POST, BOATS_PATH).hasAnyAuthority(ADMIN);
        http.authorizeRequests().antMatchers(HttpMethod.DELETE, BOATS_PATH).hasAnyAuthority(ADMIN);
        http.authorizeRequests().antMatchers(HttpMethod.PUT, BOATS_PATH).hasAnyAuthority(ADMIN);
        http.authorizeRequests().antMatchers(HttpMethod.PATCH, BOATS_PATH).hasAnyAuthority(ADMIN);
        http.authorizeRequests().antMatchers("/", "/h2-console/**", "/register/**", "/refreshToken/**").permitAll();
        http.authorizeRequests().anyRequest().authenticated();

        //ne pas creer de session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //Jwt filters
        http.addFilter(new JWTAuthenticationFilter(authenticationManager()));
        http.addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}
