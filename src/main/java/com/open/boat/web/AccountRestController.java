package com.open.boat.web;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.open.boat.entities.AppUser;
import com.open.boat.entities.RegisterForm;
import com.open.boat.entities.RoleUserForm;
import com.open.boat.security.jwt.SecurityConstants;
import com.open.boat.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class AccountRestController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/appUsers")
    public AppUser saveUser(@RequestBody AppUser appUser) {
        return accountService.saveUser(appUser);
    }

    @PostMapping("/addRoleToUser")
    public void addRoleToUser(@RequestBody RoleUserForm roleUserForm) {
        accountService.addRoleToUser(roleUserForm.getUserName(), roleUserForm.getRoleName());
    }

    @GetMapping("/profile")
    public AppUser getProfile(Principal principal) {
        return accountService.findUserByUserName(principal.getName());
    }

    @PostMapping("/register")
    public AppUser register(@RequestBody RegisterForm userForm) {
        if (!userForm.getPassword().equals(userForm.getRepassword())) {
            throw new RuntimeException("You must confirm your password");
        }

        AppUser user = accountService.findUserByUserName(userForm.getUserName());
        if (user != null) {
            throw new RuntimeException("This User already exists");
        }

        AppUser appUser = new AppUser();
        appUser.setUserName(userForm.getUserName());
        appUser.setPassword(userForm.getPassword());
        accountService.saveUser(appUser);
        accountService.addRoleToUser(userForm.getUserName(), "USER");
        return appUser;
    }

    @GetMapping("/refreshToken")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String authToken = request.getHeader(SecurityConstants.AUTHORIZATION);
        if (authToken != null && authToken.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            try {
                String jwt = authToken.substring(SecurityConstants.TOKEN_PREFIX.length());
                Algorithm algorithm = Algorithm.HMAC256(SecurityConstants.SECRET);
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String userName = decodedJWT.getSubject();
                AppUser appUser = accountService.findUserByUserName(userName);

                String jwtAcessToken = JWT.create()
                        .withSubject(appUser.getUserName())
                        .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles",
                                appUser.getRoles().stream()
                                        .map(r -> r.getRoleName())
                                        .collect(Collectors.toList()))
                        .sign(algorithm);

                String jwtRefreshToekn = JWT.create()
                        .withSubject(appUser.getUserName())
                        .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.REFRESH_EXPIRATION_TIME))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles",
                                appUser.getRoles().stream()
                                        .map(r -> r.getRoleName())
                                        .collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> idTokensMap = new HashMap<>();
                idTokensMap.put("accesstoken", SecurityConstants.TOKEN_PREFIX + jwtAcessToken);
                idTokensMap.put("refreshtoken", SecurityConstants.TOKEN_PREFIX + jwtRefreshToekn);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(), idTokensMap);

            } catch (Exception e) {
                throw e;
            }
        } else {
            throw new RuntimeException("Refresh token required !");
        }
    }
}
