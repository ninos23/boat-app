package com.open.boat.entities;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Entity
@Table(name = "Boat")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Boat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //id +1
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", length = 80, nullable = false)
    @NotBlank(message = "Name is mandatory")
    @Size(max = 80)
    private String name;

    @Column(name = "description", length = 255)
    @Size(max = 255)
    private String description;
}
