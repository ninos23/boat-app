package com.open.boat.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AppUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //id +1
    @Column(nullable = false)
    private Long id;

    @Column(unique = true, length = 80, nullable = false)
    @NotBlank(message = "userName is mandatory")
    @Size(max = 80)
    private String userName;

    @Column(length = 80, nullable = false)
    @NotBlank(message = "password is mandatory")
    @Size(max = 80)
    // pour ne pas afficher le mot de pass dans la réponse
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<AppRole> roles = new ArrayList<>();
}
