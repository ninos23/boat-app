package com.open.boat.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AppRole implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //id +1
    @Column(nullable = false)
    private Long id;

    @Column(length = 80, nullable = false)
    @NotBlank(message = "roleName is mandatory")
    @Size(max = 80)
    private String roleName;

}
