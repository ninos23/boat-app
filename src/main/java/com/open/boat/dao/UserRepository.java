package com.open.boat.dao;

import com.open.boat.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface UserRepository extends JpaRepository<AppUser, Long> {
     Optional<AppUser> findByUserName(String userName);
}
