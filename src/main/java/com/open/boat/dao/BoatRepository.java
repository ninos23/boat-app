package com.open.boat.dao;

import com.open.boat.entities.Boat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@CrossOrigin("*")
@RepositoryRestResource
public interface BoatRepository extends JpaRepository<Boat, Long> {

    //public List<Boat> findByNameStartingWith(String name);


    /*@Query("select distinct '*' from Boat b where b.name like :x ")
    public List<Boat> chercher(@Param("x")String name);*/

}
