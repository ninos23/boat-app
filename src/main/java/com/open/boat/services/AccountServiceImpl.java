package com.open.boat.services;

import com.open.boat.dao.RoleRepository;
import com.open.boat.dao.UserRepository;
import com.open.boat.entities.AppRole;
import com.open.boat.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public AppUser saveUser(AppUser user) {
        String hashPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashPassword);
        return userRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        AppRole appRole = roleRepository.findByRoleName(roleName).orElse(null);
        AppUser appUser = userRepository.findByUserName(userName).orElse(null);
        appUser.getRoles().add(appRole);
    }

    @Override
    public AppUser findUserByUserName(String userName) {
        return userRepository.findByUserName(userName).orElse(null);
    }
}
