package com.open.boat.services;

import com.open.boat.entities.AppRole;
import com.open.boat.entities.AppUser;

public interface AccountService {

    AppUser saveUser(AppUser user);

    AppRole saveRole(AppRole role);

    void addRoleToUser(String userName, String roleName);

    AppUser findUserByUserName(String userName);
}
