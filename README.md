Java 8 version
1. Clone the Project in your IDE
2. mvn clean
3. mvn package
4. Run BoatApplication as Spring boot app or run the jar generated (location : target repository) repository with "java -jar "
5. Browser link : localhost:8080
6.  Login : admin,
    Password : 1234,
    Role : Admin (POST, PUT, PATCH, DELETE)

7.  Login : user1,
    Password : 1234,
    Role : User (GET)
